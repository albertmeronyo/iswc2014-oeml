\documentclass{llncs}

\usepackage[font=footnotesize]{caption} 
\usepackage{url}
\usepackage{graphicx} 
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc}
\usepackage[multiple]{footmisc} 
\usepackage{mathtools} 
\usepackage{listings}
\usepackage[scaled=0.85]{beramono}
\usepackage{amsthm}

%\newtheorem{definition}{Definition}

\pagestyle{plain}

%\setlength{\belowcaptionskip}{-2mm}

\begin{document}

\mainmatter % start of the contributions

\title{Predicting Change in the Semantic Web}
\titlerunning{Predicting Change in the Semantic Web}

\author{Albert Mero\~{n}o-Pe\~{n}uela\inst{1,2}
\and Christophe Gu\'{e}ret\inst{2}
\and Stefan Schlobach\inst{1}
}

\authorrunning{Albert Mero\~{n}o-Pe\~{n}uela et al.}
% abbreviated author list (for running head)
%
%%%% list of authors for the TOC (use if author list has to be
%%%% modified)
\tocauthor{Albert Mero\~{n}o-Pe\~{n}uela et al.}
%
% \institute{Department of Computer Science, VU University
% Amsterdam, The
% Netherlands\\ \email{\{author\}@vu.nl}}

\institute{Department of Computer Science, VU University Amsterdam, NL
  \email{albert.merono@vu.nl} \and Data Archiving and Networked
  Services, KNAW, NL}

\maketitle
\begin{abstract}
The development and maintenance of classification schemas is a
knowledge intensive task that requires a lot of manual
effort. Librarians, historians and social scientists, among others,
need to constantly adapt these classifications to deal with the change
of meaning of concepts (i.e. \emph{concept drift}) that comes with new
data releases and novel linked datasets. In this paper we introduce a
method to automatically detect which parts of a classification schema
are likely to experience concept drift. Our approach is based on
predicting the concepts of a classification that will experience drift
in a future version by using supervised learning on features extracted
from past versions. We find that a state of the art predictor for
concept extension detection in biomedical ontologies can be adapted to
predict concept drift in classification schemes. This result is
confirmed on a variety of data sources (library, encyclopedic,
cross-domain, and socio historical data), enabling the creation of a
generic tool to assist knowledge experts in their curation tasks.

  \keywords{Semantic Web, Concept Drift, Ontology Change, Ontology
    Evolution} 
\end{abstract}

%% \section{Introduction}
%% \label{sec:intro}

%% Two research questions:
%% \begin{itemize}
%%   \item \textbf{RQ1}. Can we repliacte an existing framework for
%%     extension prediction of biomedical ontologies \cite{pesquita2012}?
%%   \item \textbf{RQ2}. Can we generalize such a framework in order to
%%     predict:
%%     \begin{itemize}
%%       \item Not only extension, but concept change and drift in
%%         general \cite{wang2010}
%%       \item Not only in biomedical ontologies, but in a variety
%%         (cross-domain, social history) of Linked Data datasets?
%%     \end{itemize}
%% \end{itemize}

%% \section{Related Work}
%% \label{sec:related-work}

%% \begin{itemize}
%%   \item Concept Drift \cite{wang2010}
%%   \item Ontology evolution \cite{pesquita2012}
%%   \item Ontology versioning
%%   \item Others?
%% \end{itemize}

%% \section{Problem Description}
%% \label{sec:problem-description}

%% There is this problem of Concept Drift. Meaning changes over time in
%% important domains such as biomedical sciences, library science and
%% historical classifications. Detection of this change is important
%% because... If correclty addressed, it could improve...

%% We want to use an existing framework for extension prediction of
%% biomedical ontologies \cite{pesquita2012} to address this concept
%% drift. 

%% First, we want to replicate it and, second, we want to see its
%% potential for the study of change in the Semantic Web, not just in
%% versioned ontologies, and not only in the biomedical domain. The
%% purpose is, hence, to generalize an existing methodology and study to
%% what extent it can be used as a generic tool to aid data curators in
%% biosciences, library sciences, social history, etc in maintaining
%% their datasets and facilitating their proactivity towards change over
%% time.

%% \section{Methodology}
%% \label{sec:methodology}

%% \subsection{Input Data}
%% \label{sec:input-data}

%% As a generalization effort of the work by Pesquita and Couto
%% \cite{pesquita2012}, we use a set of multi- and intersdisciplinary
%% Linked Data datasets for which several versions exist. Concretely, we
%% use

%% \begin{itemize}
%%   \item ?? versions of the SKOS hierarchy of DBpedia categories, plus
%%     all articles linking to those categories
%%   \item ?? versions of the DBpedia ontology, plus all articles linking
%%     to classes of such ontologies
%%   \item 8 versions of the Dutch historical censuses dataset (CEDAR),
%%     which comprises a structural 
%% \end{itemize}

%% The selection of these specific datasets is due to multiple
%% reasons. First, they cover different levels of semantic expressivity,
%% from SKOS (taxonomies) to OWL (ontologies). Second, their sizes range
%% from few terms ($10^2$) to lots of them ($10^6$). Third, the temporal
%% gap between each version varies from 10-12 months (in DBpedia
%% categories and DBpedia ontologies) to 10 years (in ithe CEDAR
%% dataset). Fourth, the selection contains both manually and
%% automatically created datasets: the DBpedia categories are
%% automatically extracted from Wikipedia; the DBpedia ontologies are
%% manually maintaned and extended by the community
%% \cite{lehmann2014} starting from the existing dumps; and the
%% CEDAR data is a totally manually maintained dataset. Fifth,
%% Wikipedia/DBpedia data is born digital and still evolving (2001--),
%% whilst the CEDAR dataset is proper Humanities legacy and temporally
%% closed (1795--1971). A full summary of the selected datasets is shown
%% in Table \ref{tab:dataset-summary}.

%% \begin{table}
%% \centering
%% \begin{tabular}{lll}
%% \hline
%% \textbf{Dataset} & \textbf{Prop1} & \textbf{Prop2} \\
%% \hline
%% foo & bar & baz \\
%% foobar & foobaz & fewz \\
%% \hline
%% \end{tabular}
%% \vspace{2mm}
%% \caption{Summary of the selected and analysed datasets.}
%% \label{tab:dataset-summary}
%% \end{table}


%% \subsection{Strategy}
%% \label{sec:strategy}

%% The idea behind our proposed approach is that the knowledge encoded in
%% past versions of a Linked Data dataset can be used to faithfully
%% predict which parts of such dataset will change in a future
%% version. This idea is heavily inspired by the work by Pesquita and
%% Couto \cite{pesquita2012} on predicting the extension of biomedical
%% ontologies. Their notion comes from change capturing strategies that
%% are based on implicit requirements. Traditionally, these requirements
%% are defined manually on an expert knowledge basis. Their approach
%% tries to learn these requirements from previous extension events using
%% supervised learning.

%% We extend this idea in three essential aspects. First, our aim is to
%% to predict not only the phenomenon of extension (e.g. a class having
%% novel children in a more recent version of the ontology), but the more
%% generic phenomenon of change. Second, our proposal runs in any kind of
%% Linked Data dataset, not only ontologies. Third, we run our
%% experiments in various datasets of very different nature (see Section
%% \ref{sec:input-data}), in an effort to study the genericity of the
%% approach and whether it is domain independent or not. The idea is to
%% propose a strategy to model and predict change in any Linked Data
%% dataset using supervised learning.

%% Features

%% Definition of change

%% \subsection{Parameters}
%% \label{sec:parameters}

%% As similarly proposed in \cite{pesquita2012}, the approach described
%% in Section \ref{sec:strategy} needs the following parameters to be
%% set:

%% \begin{itemize}
%%   \item The number of versions that will be used for training
%%   \item The deltaFC parameter: what specific snapshot will be used to decide if a concept of the training set has changed or not
%%   \item The deltaTT parameter: what specific snapshot will be used to decide if a concept of the evaluation set has changed or not
%%   \item A boolean that indicates how to deal with concepts that do not appear in all snapshots (essentially, consider it as changed, or discard it).
%%   \item The URI of the top concept in the dataset
%%   \item The URI of the predicate connecting concepts (e.g. skos:broader, or rdfs:subClassOf)
%%   \item The URI of a chosen 'membership' property (e.g. rdf:type, or dc:subject)
%% \end{itemize}

%% \subsection{Evaluation}
%% \label{sec:evaluation}

%% What we use to evaluate the quality of the learning: precision,
%% recall, f-measure, ROC curve, but also looking at the false positive
%% and false negative rates.

%% Let Ashkan evaluate on the CEDAR case, by comparing what is predicted
%% for the non-existing 1981 census and see if things really went like that.

%% \section{Experiments}
%% \label{sec:experiments}

%% \begin{itemize}
%%   \item Experiment pipeline
%%   \item Parameter permutations
%%   \item Learners used
%%   \item Hardware used
%% \end{itemize}

%% \section{Results}
%% \label{sec:results}

%% \begin{itemize}
%%   \item Classifier performance
%%   \item Best case, worst case
%%   \item Parameter combinations (e.g. plot performance against
%%     definitions of change)
%% \end{itemize}

%% \section{Discussion}
%% \label{sec:discussion}

%% \section{Conclusions and Future Work}
%% \label{sec:conclusions}


%% \vspace{3mm}

%% \scriptsize \textbf{Acknowledgements} The work on which this paper is
%% based has been partly supported by the Computational Humanities
%% Programme of the Royal Netherlands Academy of Arts and Sciences, under
%% the auspices of the CEDAR project. For further information, see
%% \url{http://ehumanities.nl}. This work has been supported by the Dutch
%% national program COMMIT.

%% \bibliographystyle{splncs03}
%% %\bibliographystyle{abbrv}
%% \bibliography{oeml}
\end{document}

